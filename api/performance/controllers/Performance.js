'use strict';

/**
 * Performance.js controller
 *
 * @description: A set of functions called "actions" for managing `Performance`.
 */

module.exports = {

  /**
   * Retrieve performance records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.performance.search(ctx.query);
    } else {
      return strapi.services.performance.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a performance record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.performance.fetch(ctx.params);
  },

  /**
   * Count performance records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.performance.count(ctx.query);
  },

  /**
   * Create a/an performance record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.performance.add(ctx.request.body);
  },

  /**
   * Update a/an performance record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.performance.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an performance record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.performance.remove(ctx.params);
  }
};
