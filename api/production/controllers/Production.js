"use strict";
var _ = require("lodash");
const Moment = require("moment");
const MomentRange = require("moment-range");

const moment = MomentRange.extendMoment(Moment);

/**
 * Production.js controller
 *
 * @description: A set of functions called "actions" for managing `Production`.
 */

module.exports = {
  /**
   * Retrieve production records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.production.search(ctx.query);
    } else {
      return strapi.services.production.fetchAll(ctx.query, populate);
    }
  },

  findByKeyword: async params => {
    const keywordMap = await Promise.all(
      params.keywords.map(
        async e => await strapi.services.production.fetchAll({ keywords: [e] })
      )
    );
    const keywordIntersection = _.intersectionBy(
      ...keywordMap,
      e => e.productionId
    );

    const startDate = params.dateRange[0].match(/([T])/g)
      ? moment(params.dateRange[0])
      : moment(params.dateRange[0]).startOf("day");
    const endDate = params.dateRange[1].match(/([T])/g)
      ? moment(params.dateRange[1])
      : moment(params.dateRange[1]).endOf("day");
    const range = moment.range(startDate, endDate);

    const result = keywordIntersection.filter(e => {
      return e.performancesDates.some(f => {
        return range.contains(moment(f));
      });
    });
    
    return result;
  },

  /**
   * Retrieve a production record.
   *
   * @return {Object}
   */

  findOne: async ctx => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.production.fetch(ctx.params);
  },

  /**
   * Count production records.
   *
   * @return {Number}
   */

  count: async ctx => {
    return strapi.services.production.count(ctx.query);
  },

  /**
   * Create a/an production record.
   *
   * @return {Object}
   */

  create: async ctx => {
    return strapi.services.production.add(ctx.request.body);
  },

  /**
   * Update a/an production record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.production.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an production record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.production.remove(ctx.params);
  }
};
